#ifndef GLOBALINC_GLOBAL_UTILS_H_
#define GLOBALINC_GLOBAL_UTILS_H_

#include <stdio.h>
#include <stdint.h>
#include "stm32f3xx_hal.h"
#include "global_defs.h"
#ifdef __cplusplus
extern "C"{
#endif

#define GP(num) (GPIO_PIN_##num)
#define WAIT_FOR_EVER 0xFFFFFFFFUL
#define ERR_BUFFER_SIZE 150
#define false 0
#define true 1

extern char msg_buffer[ERR_BUFFER_SIZE];

#define STR(x) #x

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-extra-args"
#define ERR_FATAL(msg, val1, val2, val3)								\
do{																		\
	_Pragma(STR(GCC diagnostic push))									\
	_Pragma(STR(GCC diagnostic ignored "-Wformat-extra-args"))			\
	snprintf(msg_buffer, ERR_BUFFER_SIZE, msg, val1, val2, val3);		\
	error_handler();													\
	_Pragma(STR(GCC diagnostic pop))									\
}while(false)
#pragma GCC diagnostic pop

#define str(x) (#x)

#define DO_WHILE_SUCCESS(x)						\
do{												\
	while(x != true) { vTaskDelay(10); }		\
}while(0)

#define _USED(x) ( (void) (x) )
#define ever (;;)

void error_handler();

#define ASSERT(x)																									\
do{																													\
	if(!(x))																										\
	{																												\
		snprintf(msg_buffer, ERR_BUFFER_SIZE, 																		\
		"ASSERTION [ %s ] in function %s, in line %d", str(x), __FUNCTION__, __LINE__ );							\
		error_handler();																							\
	}																												\
}while(false)

#define CEXT extern "C"
#ifdef __cplusplus
}
#endif
#endif

