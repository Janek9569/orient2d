#include <OS.hpp>
#include "clock.h"

bool OS::_is_OS_turned_on = false;

TaskHandle_t OS::addTask(void (*f)(void*), const char* name, uint16_t stackSize, void* params, uint8_t prio)
{
	TaskHandle_t newTask;
	xTaskCreate(f, name, stackSize, params, prio, &newTask);
	return newTask;
}

void OS::startScheduler()
{
	_is_OS_turned_on = true;
	clockSetTickHandler(&OS::incrementTick);
	vTaskStartScheduler();
}

extern "C"
{
	extern void xPortSysTickHandler( void );
}

void OS::incrementTick()
{
	xPortSysTickHandler();
}
