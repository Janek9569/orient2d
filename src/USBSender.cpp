#include "USBSender.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include "OS.hpp"

void USBSender::usbSender_deamon(void* argument)
{
	USBSender* usbSender = (USBSender*)argument;

	USBPacket* packet = nullptr;
	while( xQueueReceive( usbSender->msgQ, (void*)&(packet), (TickType_t)WAIT_FOR_EVER) )
	{
		ASSERT(packet != nullptr);

		CDC_Transmit_FS((uint8_t*)packet->getData(), packet->getSize());

		delete packet;
		packet = nullptr;
	}
}

//singleton class
USBSender::USBSender()
{
	static bool wasInit = false;
	ASSERT(wasInit == false);
	wasInit = true;

	msgQ = xQueueCreate( 2000, sizeof(USBPacket) );
	ASSERT(msgQ != NULL);
	OS::addTask( &USBSender::usbSender_deamon, "USBSen", 512, (void*)this, 0);
}

USBSender::~USBSender()
{

}

//return true on success
bool USBSender::pushData(USBPacket* packet)
{
	return xQueueSend(msgQ, (void*)&packet, (TickType_t)0) == pdTRUE;
}





