#include "Kalman.h"
#include "global_utils.h"

Kalman::Kalman(KalmanType type)
{
	using namespace std::placeholders;
	ASSERT(type != K_TYP_LAST);

	switch(type)
	{
	case K_TYP_2D_ORIENT:
	{
		float accWar[] = {0.5, 0.5, 0.5};
		info = get2DOrientKInfo(0.1, 0.002, accWar);
		updateTime = std::bind(&Kalman::_2DOrientUpdateTime, this, _1);
		break;
	}
	case K_TYP_YAW:
	{
		float accWar = {0.5};
		info = getYawInfo(0.1, 0.001, accWar);
		updateTime = std::bind(&Kalman::_YawUpdateTime, this, _1);
	}
	case K_TYP_LAST:
		//never there due to the ASSERT
		break;
	}

}

Kalman::~Kalman()
{
	if(info.A) { delete info.A; }
	if(info.B) { delete info.B; }
	if(info.H) { delete info.H; }
	if(info.R) { delete info.R; }
	if(info.q) { delete info.q; }
	if(info.W) { delete info.W; }
	if(info.y) { delete info.y; }
	if(info.u) { delete info.u; }
	if(info.stepInfo.pPost) { delete info.stepInfo.pPost; }
	if(info.stepInfo.xPost) { delete info.stepInfo.xPost; }
}

void Kalman::step(Matrix<float>* u, Matrix<float>* y, float dt)
{
	updateTime(dt);
	StepInfo* SInfo = &info.stepInfo;

	//Q = q*W*qT
	Matrix<float> qT = info.q->transpose();
	Matrix<float> Q = (*info.q) * (*info.W) * qT;

	//xPrio = A*xPost+B*u
	Matrix<float> AxPost = (*info.A) * (*SInfo->xPost);
	Matrix<float> Bu(1, info.A->getY());

	if(u != nullptr)
		Bu = std::move((*info.B) * (*u));

	Matrix<float> xPrio = AxPost + Bu;

	//pPrio = A*pPost*AT+Q
	Matrix<float> AT = info.A->transpose();
	Matrix<float> ApPostAT = (*info.A) * (*SInfo->pPost) * AT;
	Matrix<float> pPrio = ApPostAT + Q;

	//K = pPrio * HT * (H * pPrio * HT + R)^-1
	Matrix<float> HT = info.H->transpose();

	Matrix<float> HpPrioHT = (*info.H) * pPrio * HT;
	Matrix<float> HpPrioHTpR = HpPrioHT + (*info.R);
	Matrix<float> inv = HpPrioHTpR.inv();
	Matrix<float> K = pPrio * HT * inv;

	//newX = xPrio + K * (y - H * xPrio)
	Matrix<float> HxPrio = (*info.H) * xPrio;
	Matrix<float> ymHxPrio = (*y) - HxPrio;
	Matrix<float> KymHxPrio = K * ymHxPrio;
	Matrix<float> newX = xPrio + KymHxPrio;

	//newP = (I-K*H)*pPrio
	Matrix<float> KH = K * (*info.H);
	Matrix<float> I = Matrix<float>::I(KH.getX());
	Matrix<float> ImKH = I - KH;
	Matrix<float> newP = ImKH * pPrio;//hard f

	*SInfo->pPost = std::move(newP);
	*SInfo->xPost = std::move(newX);
}

Kalman::KalmanInfo Kalman::get2DOrientKInfo(float dt, float alfa, float accwar[2])
{
	float** d;
	Matrix<float>* A = new Matrix<float>(4,4);
	Matrix<float>* B = new Matrix<float>(2,4);
	Matrix<float>* H = new Matrix<float>(4,2);
	Matrix<float>* R = new Matrix<float>(2,2);
	Matrix<float>* q = new Matrix<float>(2,4);
	d = A->getData();
	d[0][0] = 1;	d[0][2] = -dt;
	d[1][1] = 1;	d[1][3] = -dt;
	d[2][2] = 1;
	d[3][3] = 1;
	d = H->getData();
	d[0][0] = 1;	d[1][1] = 1;
	d = R->getData();
	d[0][0] = accwar[0]*accwar[0];
	d[1][1] = accwar[1]*accwar[1];
	d = q->getData();
	d[0][0] = 1;
	d[1][1] = 1;
	d = B->getData();
	d[0][0]=dt;
	d[1][1]=dt;

	Matrix<float>* W = new Matrix<float>(Matrix<float>::I(q->getX()) * alfa);
	Matrix<float>* y = new Matrix<float>(1,2);
	Matrix<float>* u = new Matrix<float>(1,2);

	Matrix<float>* pPost = new Matrix<float>(4,4);
	Matrix<float>* xPost = new Matrix<float>(1,4);


	KalmanInfo result =
	{
		.A = A,
		.B = B,
		.H = H,
		.R = R,
		.q = q,
		.W = W,
		.y = y,
		.u = u,
		.stepInfo =
		{
			xPost,
			pPost
		}
	};
	return result;
}

Kalman::KalmanInfo Kalman::getYawInfo(float dt, float alfa, float accwar)
{
	float** d;
	Matrix<float>* A = new Matrix<float>(2,2);
	Matrix<float>* B = new Matrix<float>(1,2);
	Matrix<float>* H = new Matrix<float>(2,1);
	Matrix<float>* R = new Matrix<float>(1,1);
	Matrix<float>* q = new Matrix<float>(1,2);
	d = A->getData();
	d[0][0] = 1;	d[0][1] = -dt;
	d[1][1] = 1;
	d = H->getData();
	d[0][0] = 1;
	d = R->getData();
	d[0][0] = accwar*accwar;
	d = q->getData();
	d[0][0] = 1;
	d = B->getData();
	d[0][0]=dt;

	Matrix<float>* W = new Matrix<float>(Matrix<float>::I(q->getX()) * alfa);
	Matrix<float>* y = new Matrix<float>(1,1);
	Matrix<float>* u = new Matrix<float>(1,1);

	Matrix<float>* pPost = new Matrix<float>(2,2);
	Matrix<float>* xPost = new Matrix<float>(1,2);


	KalmanInfo result =
	{
		.A = A,
		.B = B,
		.H = H,
		.R = R,
		.q = q,
		.W = W,
		.y = y,
		.u = u,
		.stepInfo =
		{
			xPost,
			pPost
		}
	};
	return result;
}

void Kalman::_2DOrientUpdateTime(float dt)
{
	float** d = info.A->getData();
	d[0][2] = -dt;
	d[1][3] = -dt;

	d = info.B->getData();
	d[0][0]=dt;
	d[1][1]=dt;
}

void Kalman::_YawUpdateTime(float dt)
{
	float** d = info.A->getData();
	d[0][1] = -dt;

	d = info.B->getData();
	d[0][0]=dt;
}

