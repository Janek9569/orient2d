#include "stm32f3xx.h"
#include "OS.hpp"
#include "clock.h"
#include "CurrentOrientation.h"
#include "USBSender.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"

void resourceAllocator(void* argument);

int main(void)
{
  HAL_Init();
  SystemClock_Config();

  OS::addTask(resourceAllocator, "RES_ALLOC", 512, (void*)0, 0);
  OS::startScheduler();
  return 0;
}

void resourceAllocator(void* argument)
{
	MX_USB_DEVICE_Init();

	USBSender usbSender;
	CurrentOrientation co([&usbSender](CurrentOrientation::Orientation orient)->void{
		uint8_t data[USBSender::USBPacket::maxDataSize];
		int len = snprintf((char*)data, USBSender::USBPacket::maxDataSize,
				"%.6f %.6f %.6f\r\n",
				orient[CurrentOrientation::eRoll],
				orient[CurrentOrientation::ePitch],
				orient[CurrentOrientation::eYaw]);


		ASSERT(len > 0);
		ASSERT((uint32_t)len < USBSender::USBPacket::maxDataSize);
		USBSender::USBPacket* packet = new USBSender::USBPacket(data, len + 1);
		usbSender.pushData(packet);
	});

	for ever
		vTaskDelay(WAIT_FOR_EVER);
}
