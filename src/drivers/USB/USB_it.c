#include "usb_device.h"
#include "usbd_cdc_if.h"

extern PCD_HandleTypeDef hpcd_USB_FS;
void USB_HP_CAN_TX_IRQHandler(void)
{
	HAL_PCD_IRQHandler(&hpcd_USB_FS);
}

void USB_LP_CAN_RX0_IRQHandler(void)
{
	HAL_PCD_IRQHandler(&hpcd_USB_FS);
}
