#ifndef DRIVERS_DRIVER_H_
#define DRIVERS_DRIVER_H_

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include <functional>


class Driver
{
public:
	enum ISR
	{
		DMA1_CH4,
		DMA1_CH5,
		ISR_LAST
	};

	Driver();
	~Driver();
	bool lockSem(uint32_t timeout) { return xSemaphoreTake( sem, timeout ) == pdTRUE; }
	void releaseSem() { xSemaphoreGive(sem); }
	void releaseSemISR();
	static void callISRHandler(ISR isr);
	void attachToISR(ISR isr, std::function<void(void)> f);

private:
	SemaphoreHandle_t sem;

	static std::function<void(void)> ISRHandler[ISR_LAST];
};



#endif
