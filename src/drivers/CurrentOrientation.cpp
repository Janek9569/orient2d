#include "CurrentOrientation.h"
#include "OS.hpp"
#include "I2C.h"
#include "MEMS.h"
#include "global_utils.h"
#include "Matrix.h"
#include <cmath>

void CurrentOrientation::orientation_deamon(void* argument)
{
	CurrentOrientation* co = (CurrentOrientation*)argument;
	for ever
	{
		co->update(0.03);
		vTaskDelay(30);
	}
}

CurrentOrientation::CurrentOrientation(std::function<void(Orientation)> onUpdate):
	i2c(),
	mems(i2c),
	rollPitchKalman(Kalman::K_TYP_2D_ORIENT),
	yawKalman(Kalman::K_TYP_YAW),
	onUpdate(onUpdate)
{
	OS::addTask(&CurrentOrientation::orientation_deamon, "COrient", 1024, (void*)this, 0);
}

void CurrentOrientation::updateActualOrient()
{
	Matrix<float>* xP = rollPitchKalman.getXPost();
	orient[eRoll] = xP->getData()[MEMS::eX][0];
	orient[ePitch] = xP->getData()[MEMS::eY][0];

	//obtain yaw
	MEMS::DataAll lastMeasurement = mems.getLastAll();
	float xmag = lastMeasurement[MEMS::MAG][MEMS::eX];
	float ymag = lastMeasurement[MEMS::MAG][MEMS::eY];
	float zmag = lastMeasurement[MEMS::MAG][MEMS::eZ];
	float mag_norm = sqrt( (xmag*xmag)+(ymag*ymag)+(zmag*zmag) );
	xmag = xmag/mag_norm;
	ymag = ymag/mag_norm;
	zmag = zmag/mag_norm;

	float roll = orient[eRoll];
	float pitch = orient[ePitch];
	float Yh = (ymag * cos(roll)) - (zmag * sin(roll));
	float Xh = (xmag * cos(pitch)) + (ymag * sin(roll)*sin(pitch)) + (zmag * cos(roll) * sin(pitch));

	orient[eYaw] = atan2(Yh, Xh);/*
	orient[eYaw] = atan(accelerationZ/sqrt(accelerationX*accelerationX + accelerationZ*accelerationZ))/M_PI;
	orient[eYaw] = atan2( (-ymag*cos(orient[eRoll]) + zmag*sin(orient[eRoll])) ,
					(xmag*cos(orient[ePitch]) +
					ymag*sin(orient[ePitch])*sin(orient[eRoll]) +
					zmag*sin(orient[ePitch])*cos(orient[eRoll])) );*/
}

void CurrentOrientation::updateYaw()
{
	orient[eYaw] = yawKalman.getXPost()->getData()[0][0];

	orient[eYaw] = 0;
	//orient[eRoll] = 0;
	//orient[ePitch] = 0;
}

void CurrentOrientation::update(float dt)
{
	//obtain pitch and roll from kalman filter
	mems.readRawAllData();
	MEMS::DataAll data = mems.getAll_z();

	float ax = data[MEMS::ACC][MEMS::eX];
	float ay = data[MEMS::ACC][MEMS::eY];
	float az = data[MEMS::ACC][MEMS::eZ];


	orient[ePitch] = atan2(-ax, sqrt(ay*ay + az*az));

	//hax for better stability for bigger Pitch
	int sign = az > 0 ? 1 : -1;
	float miu = 0.01;
	orient[eRoll] = atan2(ay, sign*sqrt(az*az+ miu*ax*ax));

	Matrix<float>* rpY = rollPitchKalman.getY();
	rpY->getData()[MEMS::eX][0] = orient[eRoll];
	rpY->getData()[MEMS::eY][0] = orient[ePitch];

	Matrix<float>* rpU = rollPitchKalman.getU();
	rpU->getData()[MEMS::eX][0] = data[MEMS::GYR][MEMS::eX];
	rpU->getData()[MEMS::eY][0] = data[MEMS::GYR][MEMS::eY];

	rollPitchKalman.step(rpU, rpY, dt);

	updateActualOrient();

	Matrix<float>* yY = yawKalman.getY();
	yY->getData()[0][0] = orient[eYaw];

	Matrix<float>* yU = yawKalman.getU();
	yU->getData()[0][0] = data[MEMS::GYR][MEMS::eY];
	yawKalman.step(yU, yY, dt);

	updateYaw();

	onUpdate(orient);
}

CurrentOrientation::~CurrentOrientation()
{

}
