#include "I2C.h"
#include "global_utils.h"

bool I2C::isInitialised = false;

I2C::I2C()
{
	ASSERT(isInitialised == false);

	__HAL_RCC_DMA1_CLK_ENABLE();
	HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

	__HAL_RCC_I2C2_CLK_ENABLE();
	i2c_handle.Instance = I2C2;
	i2c_handle.Init.Timing = 0x2000090E;
	i2c_handle.Init.OwnAddress1 = 0;
	i2c_handle.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	i2c_handle.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	i2c_handle.Init.OwnAddress2 = 0;
	i2c_handle.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	i2c_handle.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	i2c_handle.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

	if (i2c_handle.State == HAL_I2C_STATE_RESET)
	{
	    /* Allocate lock resource and initialize it */
		i2c_handle.Lock = HAL_UNLOCKED;

	    /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
	    mspInit(&i2c_handle);
	}

	ASSERT(HAL_I2C_Init(&i2c_handle) == HAL_OK);
	ASSERT(HAL_I2CEx_ConfigAnalogFilter(&i2c_handle, I2C_ANALOGFILTER_ENABLE) == HAL_OK);
	ASSERT(HAL_I2CEx_ConfigDigitalFilter(&i2c_handle, 0) == HAL_OK);

	attachToISR(Driver::ISR::DMA1_CH4, [this]()->void {
		HAL_DMA_IRQHandler(&i2c_dma_tx);
		i2c_handle.State = HAL_I2C_STATE_READY;
		releaseSemISR();
	});

	attachToISR(Driver::ISR::DMA1_CH5, [this]()->void {
		HAL_DMA_IRQHandler(&i2c_dma_rx);
		i2c_handle.State = HAL_I2C_STATE_READY;
		releaseSemISR();
	});

	releaseSem();
	isInitialised = true;
}

void I2C::mspInit(I2C_HandleTypeDef* i2cHandle)
{
	 __HAL_RCC_GPIOF_CLK_ENABLE();
	 __HAL_RCC_GPIOA_CLK_ENABLE();
	 GPIO_InitTypeDef GPIO_InitStruct;

	 GPIO_InitStruct.Pin = GP(9) | GP(10);
	 GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	 GPIO_InitStruct.Pull = GPIO_PULLUP;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 GPIO_InitStruct.Alternate = GPIO_AF4_I2C2;
	 HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	 __HAL_RCC_I2C2_CLK_ENABLE();

	 i2c_dma_rx.Instance = DMA1_Channel5;
	 i2c_dma_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	 i2c_dma_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	 i2c_dma_rx.Init.MemInc = DMA_MINC_ENABLE;
	 i2c_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	 i2c_dma_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	 i2c_dma_rx.Init.Mode = DMA_NORMAL;
	 i2c_dma_rx.Init.Priority = DMA_PRIORITY_LOW;
	 ASSERT(HAL_DMA_Init(&i2c_dma_rx) == HAL_OK);
     __HAL_LINKDMA(i2cHandle, hdmarx, i2c_dma_rx);

     i2c_dma_tx.Instance = DMA1_Channel4;
     i2c_dma_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
     i2c_dma_tx.Init.PeriphInc = DMA_PINC_DISABLE;
     i2c_dma_tx.Init.MemInc = DMA_MINC_ENABLE;
     i2c_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
     i2c_dma_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
     i2c_dma_tx.Init.Mode = DMA_NORMAL;
     i2c_dma_tx.Init.Priority = DMA_PRIORITY_LOW;
	 ASSERT(HAL_DMA_Init(&i2c_dma_tx) == HAL_OK);
	 __HAL_LINKDMA(i2cHandle, hdmatx, i2c_dma_tx);
}

I2C::~I2C()
{
	__HAL_RCC_I2C2_CLK_DISABLE();
	HAL_GPIO_DeInit(GPIOA, GP(9) | GP(10));

    HAL_DMA_DeInit(i2c_handle.hdmarx);
    HAL_DMA_DeInit(i2c_handle.hdmatx);
}

void I2C::write(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size,uint8_t* data, uint16_t len)
{
	lockSem(WAIT_FOR_EVER);
	while(HAL_I2C_Mem_Write_DMA(&i2c_handle, dev_addr, mem_addr, mem_size, data, len) != HAL_OK)
		vTaskDelay(1);
	lockSem(WAIT_FOR_EVER);
	releaseSem();
}

void I2C::read(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len)
{
	lockSem(WAIT_FOR_EVER);
	while(HAL_I2C_Mem_Read_DMA(&i2c_handle, dev_addr, mem_addr, mem_size, data, len)!= HAL_OK)
		vTaskDelay(1);
	lockSem(WAIT_FOR_EVER);
	releaseSem();
}

void I2C::readBlocking(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len, uint32_t timeout)
{
	lockSem(WAIT_FOR_EVER);
	HAL_I2C_Mem_Read(&i2c_handle, dev_addr, mem_addr, mem_size, data, len, timeout);
	releaseSem();
}

void I2C::writeBlocking(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len, uint32_t timeout)
{
	lockSem(WAIT_FOR_EVER);
	HAL_I2C_Mem_Write(&i2c_handle, dev_addr, mem_addr, mem_size, data, len, timeout);
	releaseSem();
}

void I2C::write(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t data)
{
	write(dev_addr, mem_addr, mem_size, &data, 1);
}

void I2C::writeBlocking(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t data, uint32_t timeout)
{
	writeBlocking(dev_addr, mem_addr, mem_size, &data, 1, timeout);
}









