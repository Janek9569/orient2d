#ifndef DRIVERS_I2C_I2C_H_
#define DRIVERS_I2C_I2C_H_

#include "global_utils.h"
#include "stm32f3xx_hal.h"
#include "Driver.h"

//TODO: remove singleton class
class I2C : private Driver
{
public:
	I2C();
	~I2C();
	void write(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size,uint8_t* data, uint16_t len);
	void read(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len);
	void readBlocking(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t* data, uint16_t len, uint32_t timeout);
	void writeBlocking(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size,uint8_t* data, uint16_t len, uint32_t timeout);

	void write(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t data);
	void writeBlocking(uint16_t dev_addr, uint16_t mem_addr, uint16_t mem_size, uint8_t data, uint32_t timeout);

private:
	I2C_HandleTypeDef i2c_handle;
	DMA_HandleTypeDef i2c_dma_rx;
	DMA_HandleTypeDef i2c_dma_tx;

	static bool isInitialised;
	void mspInit(I2C_HandleTypeDef* i2cHandle);
};


#endif
