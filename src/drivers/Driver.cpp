#include "Driver.h"
#include "global_utils.h"
#include "stm32f3xx_hal.h"

std::function<void(void)> Driver::ISRHandler[ISR_LAST];

Driver::Driver()
{
	sem = xSemaphoreCreateBinary();
}

void Driver::releaseSemISR()
{
	BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(sem, &xHigherPriorityTaskWoken);
}

void Driver::attachToISR(ISR isr, std::function<void(void)> f)
{
	ISRHandler[isr] = f;
}

void Driver::callISRHandler(ISR isr)
{
	ISRHandler[isr]();
}

/*****INTS*****/
CEXT void DMA1_Channel5_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH5);
}

CEXT void DMA1_Channel4_IRQHandler(void)
{
	Driver::callISRHandler(Driver::ISR::DMA1_CH4);
}
