#include "MEMS.h"
#include "global_utils.h"
/*!! Call from task context!!*/
MEMS::MEMS(I2C& i2c):
	i2c(i2c)
{
	//select clock source
	i2c.write(memsAddr, PWR_MGMT_1, 1, 0x01);

	//enable master I2C mode
	i2c.write(memsAddr, USER_CTRL, 1, 0x20);

	//set i2c speed to 400kHz
	i2c.write(memsAddr, I2C_MST_CTRL, 1, 13);

	//i2c communication test
	uint8_t whoAmIReadValue = 0x00;
	i2c.read(memsAddr, WHO_AM_I, 1, &whoAmIReadValue, 1);
	ASSERT(whoAmIReadValue == mpuWhoAmIValue);

	//resolution
	i2c.write(memsAddr, ACCEL_CONFIG, 1, defaultAccResolutionCFG);
	i2c.write(memsAddr, GYRO_CONFIG, 1, defaultGyrResolutionCFG);

	#ifdef MEMS_HANDLED_WITH_INTS
	    //enable MPU interrupts
		i2c.write(memsAddr, INT_ENABLE, 1, 0x40);

	    //set threshold
	    i2c.write(memsAddr, WOM_THR, 1, 3);

	    //TO DO configure int period
	#endif

	//int pin active - H , PP, 50us pulse, bypass magnet I2C interface
	i2c.write(memsAddr, INT_PIN_CFG, 1, 1);

	//configure magnetometer
	//power down magnet
	magnetWrite(CNTL, 0x00);
	vTaskDelay(10);

	//eneter fuse rom
	magnetWrite(CNTL, 0x0F);
	vTaskDelay(10);

	//read calibration data
	uint8_t calibrationData[eDirLast] = { 0 };
	magnetRead(ASAX, calibrationData, eDirLast);

	//power down magnet
	magnetWrite(CNTL, 0x00);
	vTaskDelay(10);

	magnetCalibrationValues[eX] =  (float)(calibrationData[eX] - 128)/256. + 1.;
	magnetCalibrationValues[eY] =  (float)(calibrationData[eY] - 128)/256. + 1.;
	magnetCalibrationValues[eZ] =  (float)(calibrationData[eZ] - 128)/256. + 1.;

	//set cntl reg -> set continuous measurement 1 (100Hz) and 16bit output
	magnetWrite(CNTL, 0x16);
	vTaskDelay(10);

	magnetRead(WIA, whoAmIReadValue);
	ASSERT(whoAmIReadValue == magWhoAmIValue);

	Data accSum;
	for(uint16_t i = 0; i < gyrAverageLoops; ++i)
	{
		accSum += readRawGyrData();
		vTaskDelay(1);
	}
	zeroPoint[GYR] = accSum / gyrAverageLoops;

	zeroPoint[MAG][eX] = 11.5;
	zeroPoint[MAG][eY] = 0;
	zeroPoint[MAG][eZ] = -21;

	magScale[eX] = 0.994431376;
	magScale[eY] = 0.990474522;
	magScale[eZ] = 1.01545215;

	//megnetCalibration();
}

void MEMS::megnetCalibration()
{
	int16_t max[eDirLast] = {-INT16_MAX, -INT16_MAX, -INT16_MAX};
	int16_t min[eDirLast] = {INT16_MAX, INT16_MAX, INT16_MAX};

	for(uint32_t i = 0; i < magAverageLoops; ++i)
	{
		Data actData = readRawMagData();
		for(uint32_t d = 0; d < (uint32_t)eDirLast; ++d)
		{
			if(actData[(eDir)d] > max[(eDir)d])
				max[(eDir)d] = actData[(eDir)d];

			if(actData[(eDir)d] < min[(eDir)d])
				min[(eDir)d] = actData[(eDir)d];
		}
		vTaskDelay(12);
	}

	//hard iron correction
	zeroPoint[MAG][eX] = (max[(eDir)eX] + min[(eDir)eX])/2.0;
	zeroPoint[MAG][eY] = (max[(eDir)eY] + min[(eDir)eY])/2.0;
	zeroPoint[MAG][eZ] = (max[(eDir)eZ] + min[(eDir)eZ])/2.0;

	zeroPoint[MAG][eX] = zeroPoint[MAG][eX];
	zeroPoint[MAG][eY] = zeroPoint[MAG][eY];
	zeroPoint[MAG][eZ] = zeroPoint[MAG][eZ];

	// Get soft iron correction estimate
	magScale[eX]  = (max[0] - min[0]) / 2;  // get average x axis max chord length in counts
	magScale[eY]  = (max[1] - min[1]) / 2;  // get average y axis max chord length in counts
	magScale[eZ]  = (max[2] - min[2]) / 2;  // get average z axis max chord length in counts

	float averange = (magScale[eX] + magScale[eY] + magScale[eZ]) / 3.0;

	magScale[eX] = averange / magScale[eX];
	magScale[eY] = averange / magScale[eY];
	magScale[eZ] = averange / magScale[eZ];
}

void MEMS::magnetWrite(MAG_REG reg, uint8_t data)
{
	//set slave0 to magnetometer with writing mode
	i2c.write(memsAddr, I2C_SLV0_ADDR, 1, 0x0C);

	//set desinantion reg
	i2c.write(memsAddr, I2C_SLV0_REG, 1, reg);

	//set data buffer
	i2c.write(memsAddr, I2C_SLV0_DO, 1, data);

	//enable i2c
	i2c.write(memsAddr, I2C_SLV0_CTRL, 1, 0x81);

	//confirm if writing was successfull
	uint8_t confirmData = 0x00;
	magnetRead(reg, confirmData);
	ASSERT(confirmData == data);
}

void MEMS::magnetRead(MAG_REG reg, uint8_t& data)
{
	magnetRead(reg, &data, 1);
}

void MEMS::magnetRead(MAG_REG reg, uint8_t* data, uint8_t len)
{
	ASSERT(len <= 0x0F);

	//set slave0 to magnetometer with reading mode
	i2c.write(memsAddr, I2C_SLV0_ADDR, 1, 0x8C);

	//set source reg
	i2c.write(memsAddr, I2C_SLV0_REG, 1, reg);

	//request for 1 byte
	i2c.write(memsAddr, I2C_SLV0_CTRL, 1, 0x80 | len);

	//wait for data transfer
	vTaskDelay(1);

	//read magnet data from mpu
	i2c.read(memsAddr, EXT_SENS_DATA_00, 1, data, len);
}

MEMS::Data MEMS::readRawGyrData()
{
	MEMS::Data measurement;

	uint8_t data[6] = { 0 };
	i2c.read(memsAddr, GYRO_XOUT_H, 1, data, 6);
	int16_t X_as=(data[0]<<8 | data[1]);
	measurement[eX] = ((float)X_as*defaultGyrResolution * Deg2RadFactor)/(float)INT16_MAX;
	int16_t Y_as=(data[2]<<8 | data[3]);
	measurement[eY] = ((float)Y_as*defaultGyrResolution * Deg2RadFactor)/(float)INT16_MAX;
	int16_t Z_as=(data[4]<<8 | data[5]);
	measurement[eZ] = ((float)Z_as*defaultGyrResolution * Deg2RadFactor)/(float)INT16_MAX;

	lastMeasurement[GYR] = measurement;
	return measurement;
}

MEMS::Data MEMS::readRawMagData()
{
	MEMS::Data measurement;
	uint8_t data[6] = { 0 };
	magnetRead(HXL, data, 6);

	uint8_t statusReg = 0;
	magnetRead(ST2, statusReg);

	//magnetRead(HXL, data, 6);
	int16_t X_m=(data[0]<<8 | data[1]);
	measurement[eX] = ((float)X_m * magnetScalar * magnetCalibrationValues[eX]);
	int16_t Y_m=(data[2]<<8 | data[3]);
	measurement[eY] = ((float)Y_m * magnetScalar * magnetCalibrationValues[eY]);
	int16_t Z_m=(data[4]<<8 | data[5]);
	measurement[eZ] = ((float)Z_m * magnetScalar * magnetCalibrationValues[eZ]);

	lastMeasurement[MAG] = measurement;
	return measurement;
}

MEMS::Data MEMS::readRawAccData()
{
	MEMS::Data measurement;

	uint8_t data[6] = { 0 };
	i2c.read(memsAddr, ACCEL_XOUT_H, 1, data, 6);
	int16_t X_acc=(data[0]<<8 | data[1]);
	measurement[eX] = X_acc; //((float)X_acc*defaultAccResolution*gAcc)/(float)INT16_MAX;
	int16_t Y_acc=(data[2]<<8 | data[3]);
	measurement[eY] = Y_acc;//((float)Y_acc*defaultAccResolution*gAcc)/(float)INT16_MAX;
	int16_t Z_acc=(data[4]<<8 | data[5]);
	measurement[eZ] = Z_acc;//((float)Z_acc*defaultAccResolution*gAcc)/(float)INT16_MAX;

	lastMeasurement[ACC] = measurement;
	return measurement;
}

MEMS::Data MEMS::getGyro_z() const
{
	return lastMeasurement[GYR] - zeroPoint[GYR];
}

MEMS::Data MEMS::getAcc_z() const
{
	return lastMeasurement[ACC] - zeroPoint[ACC];
}

MEMS::Data MEMS::getMagn_z() const
{
	return (lastMeasurement[MAG] - zeroPoint[MAG]) * magScale;
}

MEMS::DataAll MEMS::getAll_z() const
{
	return MEMS::DataAll(
			lastMeasurement[GYR] - zeroPoint[GYR],
			lastMeasurement[ACC] - zeroPoint[ACC],
			lastMeasurement[MAG] - zeroPoint[MAG]);
}

MEMS::DataAll MEMS::readRawAllData()
{
	return MEMS::DataAll(
			readRawGyrData(),
			readRawAccData(),
			readRawMagData()
			);
}

MEMS::Data MEMS::getLastGyro() const
{
	return lastMeasurement[MEMS::GYR];
}

MEMS::Data MEMS::getLastAcc() const
{
	return lastMeasurement[MEMS::ACC];
}

MEMS::Data MEMS::getLastMagn() const
{
	return lastMeasurement[MEMS::MAG];
}

MEMS::DataAll MEMS::getLastAll() const
{
	return MEMS::DataAll(
			lastMeasurement[MEMS::GYR],
			lastMeasurement[MEMS::ACC],
			lastMeasurement[MEMS::MAG]
			);
}

MEMS::Data::Data(const Data& d)
{
	memcpy(data, d.data, sizeof(float) * eDirLast);
}

MEMS::Data MEMS::Data::operator/ (const Data& d) const
{
	Data result(*this);
	result[eX] /= d[eX];
	result[eY] /= d[eY];
	result[eZ] /= d[eZ];
	return result;
}

MEMS::Data MEMS::Data::operator* (const Data& d) const
{
	Data result(*this);
	result[eX] *= d[eX];
	result[eY] *= d[eY];
	result[eZ] *= d[eZ];
	return result;
}

MEMS::Data MEMS::Data::operator+ (const Data& d) const
{
	Data result(*this);
	result[eX] += d[eX];
	result[eY] += d[eY];
	result[eZ] += d[eZ];
	return result;
}

MEMS::Data MEMS::Data::operator- (const Data& d) const
{
	Data result(*this);
	result[eX] -= d[eX];
	result[eY] -= d[eY];
	result[eZ] -= d[eZ];
	return result;
}

MEMS::Data MEMS::Data::operator/= (const Data& d)
{
	return *this =  (*this) / d;
}

MEMS::Data MEMS::Data::operator*= (const Data& d)
{
	return *this =  (*this) * d;
}

MEMS::Data MEMS::Data::operator+= (const Data& d)
{
	return *this = (*this) + d;
}

MEMS::Data MEMS::Data::operator-= (const Data& d)
{
	return *this = (*this) - d;
}

MEMS::Data MEMS::Data::operator/ (const float& f) const
{
	Data result(*this);
	result[eX] /= f;
	result[eY] /= f;
	result[eZ] /= f;
	return result;
}

MEMS::Data MEMS::Data::operator* (const float& f) const
{
	Data result(*this);
	result[eX] *= f;
	result[eY] *= f;
	result[eZ] *= f;
	return result;
}




