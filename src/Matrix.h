#ifndef MATRIX_H_
#define MATRIX_H_

#include "global_utils.h"
#include "FreeRTOS.h"
#include <stdint.h>
#include <string.h>

/*Constructor for T is not called automatically!*/
//TODO:Matrix add ASSERTS
//TODO:Matrix add T allocator
//TODO:Matrix add fault flag
template<typename T>
class Matrix
{
public:
	Matrix(const Matrix<T>&) = delete;

	Matrix(Matrix<T>&& movedMatrix):
		x(movedMatrix.x), y(movedMatrix.y), data(movedMatrix.data)
	{
		movedMatrix.x = 0;
		movedMatrix.y = 0;
		movedMatrix.data = nullptr;
	}

	Matrix<T>& operator=(Matrix<T>&& rhs)
	{
		using std::swap;
		swap(data, rhs.data);
		swap(y, rhs.y);
		swap(x, rhs.x);
		return *this;
    }

	Matrix(uint32_t x, uint32_t y)
	{
		uint32_t j = 0;

		this->x = x;
		this->y = y;
		if ( (data = (float**)pvPortMalloc( y*sizeof(T))) == NULL )
			return;

		for (uint32_t i = 0; i < y; ++i)
		{
			if ( (data[i] = (float*)pvPortMalloc(x*sizeof(T))) == NULL )
			{
				//free currently allocated rows
				for(j = 0; j < i; ++j)
					vPortFree(data[j]);
				vPortFree(data);
				return;
			}
			memset(data[i], 0x00, x*sizeof(T));
		}
	}

	~Matrix()
	{
		for(uint32_t i = 0; i < y; ++i)
			vPortFree(data[i]);

		vPortFree(data);
	}

	void *operator new(size_t size)
	{
		void *p = pvPortMalloc(size);
		return p;
	}

	void operator delete(void *p)
	{
		vPortFree(p);
	}

	T* operator[](size_t i)
	{
		return data[i];
	}
	const T* operator[](size_t i) const
	{
		return data[i];
	}

	void multiple_s(const Matrix<T>& m2, Matrix<T>& result) const
	{
		ASSERT(y == m2.x);
		ASSERT(result.y == y);
		ASSERT(result.x == m2.x);

		T sum = 0;
		/*by rows*/
		for(uint32_t i = 0; i < y; ++i)
		{	/*by columns*/
			for(uint32_t j = 0; j < m2.x; ++j)
			{
				sum = 0;
				/*sum of multiplication of row*col*/
				for(uint32_t k = 0; k< x; ++k)
					sum += data[i][k] * m2.data[k][j];

				result.data[i][j] = sum;
			}
		}
	}

	Matrix<T> multiple(const Matrix<T>& m2) const
	{
		Matrix<T> result(m2.x, y);
		T sum = 0;
		for(uint32_t i = 0; i < y; ++i)
		{
			for(uint32_t j = 0; j < m2.x; ++j)
			{
				sum = 0;

				for(uint32_t k = 0; k < x; ++k)
					sum += data[i][k] * m2.data[k][j];

			result.data[i][j] = sum;
			}
		}
		return result;
	}

	Matrix<T> operator*(const Matrix<T>& m2) const
	{
		return multiple(m2);
	}

	Matrix<T> operator*(const T scalar) const
	{
		Matrix<T> result(x, y);
		for(uint32_t i = 0; i < x; ++i)
			for(uint32_t j = 0; j < y; ++j)
				result.data[i][j] = data[i][j]*scalar;
		return result;
	}

	Matrix<T> operator=(const Matrix<T>& m)
	{
		Matrix<T> result(m.x, m.y);
		for(uint32_t i = 0; i < m.x; ++i)
			for(uint32_t j = 0; j < m.y; ++j)
				result.data[i][j] = m.data[i][j];
		return result;
	}

	Matrix<T> add(const Matrix<T>& m2) const
	{
		ASSERT(x == m2.x);
		ASSERT(y == m2.y);

		Matrix<T> result(x, y);
		/*by rows*/
		for(uint32_t i = 0; i < y; ++i)
			/*by columns*/
			for(uint32_t j = 0; j <x; ++j)
				result.data[i][j] = data[i][j] + m2.data[i][j];

		return result;
	}

	Matrix<T> operator+(const Matrix<T>& m2) const
	{
		return add(m2);
	}

	void add_s(const Matrix<T>& m2, Matrix<T>& result) const
	{
		ASSERT(x == m2.x);
		ASSERT(y == m2.y);

		/*by rows*/
		for(uint32_t i = 0; i < y;++i)
			/*by columns*/
			for(uint32_t j = 0; j < x; ++j)
				result.data[i][j] = data[i][j] + m2.data[i][j];
	}

	Matrix<T> transpose() const
	{
		Matrix<T> result(y, x);
		/*by rows*/
		for(uint32_t i = 0; i < y;++i)
			/*by columns*/
			for(uint32_t j = 0; j < x; ++j)
				result.data[j][i] = data[i][j];

		return result;
	}

	void transpose_s(Matrix<T>& result) const
	{
		/*by rows*/
		for(uint32_t i = 0; i < y;++i)
			/*by columns*/
			for(uint32_t j = 0; j < x; ++j)
				result.data[j][i] = data[i][j];
	}

	Matrix<T> substract(const Matrix<T>& m2) const
	{
		ASSERT(x == m2.x);
		ASSERT(y == m2.y);

		Matrix<T> result(x, y);
		/*by rows*/
		for(uint32_t i = 0; i < y; ++i)
			/*by columns*/
			for(uint32_t j = 0; j < x; ++j)
				result.data[i][j] = data[i][j] - m2.data[i][j];

		return result;
	}

	Matrix<T> operator-(const Matrix<T>& m2) const
	{
		return substract(m2);
	}

	void substract_s(const Matrix<T>& m2, Matrix<T>& result) const
	{
		ASSERT(x == m2.x);
		ASSERT(y == m2.y);

		/*by rows*/
		for(uint32_t i = 0; i < y;++i)
			/*by columns*/
			for(uint32_t j = 0; j < x; ++j)
				result.data[i][j] = data[i][j] - m2.data[i][j];
	}

	//calc inverse by splitting with I matrix
	Matrix<T> inv() const
	{
		ASSERT(x == y);

		Matrix<T> calc(x*2, y);
		if(x == 1)
		{
			calc.data[0][0] = 1 / data[0][0];
			return calc;
		}

		/*copy m and apply I matrix*/
		for(uint32_t i = 0; i < x; ++i)
		{
			calc.data[i][i+y] = 1;
			for(uint32_t j = 0; j < y; ++j)
				calc.data[i][j] = data[i][j];
		}

		for(uint32_t i = 0; i < calc.y; ++i)
		{
			calc.swapWithBiggestRow(i);
			/*peculiar matrix*/
			if(calc.data[i][i] == 0)
			{
				//TODO: set fault flag
				return calc;
			}

			for(uint32_t j = 1 + i; j < calc.y; ++j)
			{
				T factor = calc.data[j][i] / calc.data[i][i];

				/*substract rows*/
				for(uint32_t k = 0; k < calc.x; ++k)
					calc.data[j][k] = calc.data[j][k] - (calc.data[i][k] * factor);
			}
		}

		for(uint32_t i = calc.y; i-1 != 0; --i)
		{
			/*peciliar matrix*/
			if(calc.data[i-1][i-1] == 0)
			{
				//TODO: set fault flag
				return calc;
			}

			for(uint32_t j = i - 1; j != 0; --j)
			{
				T factor = calc.data[j-1][i-1] / calc.data[i-1][i-1];

				/*substract rows*/
				for(uint32_t k = 0; k < calc.x; ++k)
					calc.data[j-1][k] = calc.data[j-1][k] - (calc.data[i-1][k] * factor);
			}
		}

		/*normalize diag*/
		for(uint32_t i = 0; i < calc.y; ++i)
		{
			if(calc.data[i][i] != 1)
			{
				T factor = 1 / calc.data[i][i];
				for(uint32_t k = 0; k < calc.x; ++k)
					calc.data[i][k] *=  factor;
			}
		}

		/*extract inverse matrix*/
		Matrix<T> result(x, y);
		for(uint32_t i = 0; i < result.x; ++i)
			for(uint32_t j = 0; j < result.y; ++j)
				result.data[i][j] = calc.data[i][j+y];

		return result;
	}

	static Matrix<T> I(uint32_t size)
	{
		//we assume that initial value for Matrix entry equals to logical zero
		Matrix<T> result(size, size);

		for(uint32_t i = 0; i < size; ++i)
			result.data[i][i] = 1;

		return result;
	}

	uint32_t getX() const { return x; }
	uint32_t getY() const { return y; }
	T** getData() const { return data; }
private:
	void swapWithBiggestRow(uint32_t i)
	{
		T biggest = 0;
		uint32_t biggestNum = 0;

		for(uint32_t k = i; k < y; ++k)
		{
			T temp = data[k][i];

			if(temp < 0)
				temp *= (-1);

			if(temp > biggest)
			{
				biggest = temp;
				biggestNum = k;
			}
		}

		T oldValue = 0;

		/*swap rows i , biggest_num	*/
		for(uint32_t k = 0; k < x; ++k)
		{
			oldValue = data[i][k];
			data[i][k] = data[biggestNum][k];
			data[biggestNum][k] = oldValue;
		}
	}

	uint32_t x,y;
	T** data;
};

#endif /* MATRIX_H_ */
