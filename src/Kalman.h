#ifndef KALMAN_H_
#define KALMAN_H_

#include <functional>
#include "Matrix.h"

class Kalman
{
public:
	enum KalmanType
	{
		K_TYP_2D_ORIENT,
		K_TYP_YAW,
		K_TYP_LAST
	};

	Kalman(KalmanType type);
	~Kalman();
	void step(Matrix<float>* u, Matrix<float>* y, float dt);
	Matrix<float>* getU() { return info.u; }
	Matrix<float>* getY() { return info.y; }
	Matrix<float>* getXPost() { return info.stepInfo.xPost; }
private:
	struct StepInfo
		{
			Matrix<float>* xPost;
			Matrix<float>* pPost;
		};

		struct KalmanInfo
		{
			Matrix<float>* A;
			Matrix<float>* B;
			Matrix<float>* H;
			Matrix<float>* R;
			Matrix<float>* q;
			Matrix<float>* W;

			//for computations
			Matrix<float>* y;
			Matrix<float>* u;

			StepInfo stepInfo;
		};

	KalmanInfo info;
	//A, B, dt
	std::function<void(float)> updateTime;
	KalmanInfo get2DOrientKInfo(float dt, float alfa, float accwar[2]);
	KalmanInfo getYawInfo(float dt, float alfa, float accwar);
	void _2DOrientUpdateTime(float dt);
	void _YawUpdateTime(float dt);
};

#endif
