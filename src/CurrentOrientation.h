#ifndef CURRENTORIENTATION_H_
#define CURRENTORIENTATION_H_

#include <cstring>
#include <functional>
#include "Kalman.h"
#include "I2C.h"
#include "MEMS.h"

class CurrentOrientation
{

public:
	enum eOrient
	{
		eRoll,
		ePitch,
		eYaw,
		eOrientLast
	};

	struct Orientation
	{
		Orientation() { memset(orient, 0x00, sizeof(float) * eOrientLast); }

		float& operator[](eOrient o)
		{
			ASSERT(o != eOrientLast);
			return orient[o];
		}
		const float& operator[](eOrient o) const
		{
			ASSERT(o != eOrientLast);
			return orient[o];
		}

	private:
		float orient[eOrientLast];
	};

	CurrentOrientation(std::function<void(Orientation)> onUpdate);
	~CurrentOrientation();

	void update(float dts);
	Orientation getActualOrientation() const { return orient; }
private:
	I2C i2c;
	MEMS mems;
	Orientation orient;
	Kalman rollPitchKalman;
	Kalman yawKalman;
	std::function<void(Orientation)> onUpdate;

	void updateActualOrient();
	void updateYaw();

	static void orientation_deamon(void* argument);
};


#endif
