#ifndef CLOCK_H_
#define CLOCK_H_

#ifdef __cplusplus
extern "C"
{
#endif

	void SystemClock_Config(void);
	void clockSetTickHandler(void (*f)(void));

#ifdef __cplusplus
}
#endif
#endif
